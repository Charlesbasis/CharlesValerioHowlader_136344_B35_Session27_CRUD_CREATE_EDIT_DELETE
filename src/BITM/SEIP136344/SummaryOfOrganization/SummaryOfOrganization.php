<?php


namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class SummaryOfOrganization extends DB{

    public $id="";

    public $company_name="";

    public $summary="";



    public function __construct(){

        parent::__construct();

    }

//    public function index(){
//        echo "SummaryOfOrganization found!";
//    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('company_name',$postVariabledata)){
            $this->company_name = $postVariabledata['company_name'];
        }

        if(array_key_exists('summary',$postVariabledata)){
            $this->summary = $postVariabledata['summary'];
        }
    }

    public function store(){

        $arrData = array($this->company_name, $this->summary);

        $sql = "Insert INTO summary_of_organization(company_name,summary) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from summary_of_organization');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from summary_of_organization where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of index();


}


